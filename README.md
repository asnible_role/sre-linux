# Ansible Role: Alpine

An Ansible Role that updates and upgrades Linux.

## Requirements

None.

## Role Variables

Available variables are listed below, along with default values:
```yaml
repo: /etc/apk/
```

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - sre-linux

## License

MIT / BSD

## Author Information

This role was created in 2020 by [Nm@w](https://gitlab.com/nmaw).